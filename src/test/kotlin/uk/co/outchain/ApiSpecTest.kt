package uk.co.outchain

import org.junit.jupiter.api.Test
import java.io.File
import org.junit.jupiter.api.Assertions.*
import uk.co.outchain.http.*

class ApiSpecTest {
    private val resDir = "src/test/resources/uk/co/outchain"

    data class DocumentResponse(
        @PropertySpec(
            required = true,
            example = "5bac7c3f792f2a480e7d169c"
        )
        val id: String,

        @PropertySpec(
            required = true,
            type = "string",
            enum = [
                "scheduled - Задача добавлена в очередь для выполнения",
                "processing - Документ шифруется",
                "finished - Документ успешно зашифрован"
            ]
        )
        val status: String,

        @PropertySpec(
            required = true,
            description = "Является ли документ публичным"
        )
        val is_public: Boolean,

        @PropertySpec(
            description = "Путь до документа на сервере (только в дебаг режиме)"
        )
        val debug_server_path: String? = null,

        @PropertySpec(
            description = "Мастер ключи для дешифровки, null, если документ публичный",
            array = MasterKeyResponse::class
        )
        val master_keys: List<MasterKeyResponse>? = null
    )

    data class MasterKeyResponse(
        @PropertySpec(
            required = true,
            description = "Владелец ключа"
        )
        val identity: String
    )

    data class UploadRequestBody(
        @PropertySpec(
            required = true,
            obj = MetaRequestBody::class
        )
        val meta: MetaRequestBody,

        @PropertySpec(
            required = true,
            arrayType = "string",
            arrayFormat = "binary",
            description = "Публичные ключи",
            fieldName = "public_keys[]"
        )
        val public_keys: List<String>,

        @PropertySpec(
            required = true,
            arrayType = "string",
            arrayFormat = "binary",
            description = "Документы",
            fieldName = "documents[]"
        )
        val documents: List<String>
    )

    data class MetaRequestBody(
        @PropertySpec(
            required = true,
            description = "Публичные ключи",
            array = PublicKeyMetaRequestBody::class
        )
        val public_keys: List<PublicKeyMetaRequestBody>
    )

    data class PublicKeyMetaRequestBody(
        @PropertySpec(
            required = true,
            example = "user-1"
        )
        val identity: String,

        @PropertySpec(
            required = true,
            example = "user-1-public_key"
        )
        val file_name: String
    )

    @Test
    fun test() {
        val spec = ApiSpec(
            description = "Сервис для загрузки и шифрования документов",
            version = "1.0.0",
            title = "RecyclerOS Documents Service",
            servers = listOf(
                ApiSpecServer(url = "https://127.0.0.1:8080/v1/")
            ),
            methods = mutableListOf(
                ApiSpecMethod(
                    url = "/get-document-by-sha256/:hash",
                    method = ApiMethod.GET,
                    tags = listOf("document"),
                    summary = "Получение документа по хеш сумме",
                    parameters = listOf(
                        SpecParameter(
                            name = "hash",
                            type = "string",
                            description = "SHA-256 хеш сумма документа"
                        )
                    ),
                    responses = listOf(
                        ApiSpecResponse(
                            statusCode = 200,
                            description = "Успешеная операция",
                            content = ApiSpecContent(
                                contentType = "application/json",
                                schema = DocumentResponse::class
                            )
                        )
                    ),
                    errors = listOf(
                        ApiError.FailToRetrieveDocument.spec
                    )
                ),
                ApiSpecMethod(
                    url = "/get-master-key-id/:id",
                    method = ApiMethod.GET,
                    tags = listOf("document"),
                    summary = "Получение ключа по id",
                    parameters = listOf(
                        SpecParameter(
                            name = "id",
                            type = "string",
                            description = "Уникальный идентификатор документа"
                        )
                    ),
                    responses = listOf(
                        ApiSpecResponse(
                            statusCode = 200,
                            description = "Успешеная операция",
                            content = ApiSpecContent(
                                contentType = "application/xml",
                                schema = MasterKeyResponse::class
                            )
                        )
                    ),
                    errors = listOf(
                        ApiError.FailToRetrieveDocument.spec
                    )
                ),
                ApiSpecMethod(
                    url = "/upload-document",
                    method = ApiMethod.POST,
                    tags = listOf("document"),
                    summary = "Загрузка документов",
                    requestBody = ApiSpecContent(
                        contentType = "multipart/form-data",
                        schema = UploadRequestBody::class
                    ),
                    responses = listOf(
                        ApiSpecResponse(
                            statusCode = 200,
                            description = "Успешеная операция",
                            content = ApiSpecContent(
                                contentType = "application/json",
                                schema = DocumentResponse::class
                            )
                        )
                    ),
                    samples = listOf(
                        ApiSpecCodeSample(
                            "python",
                            "test-python.py"
                        )
                    ),
                    errors = listOf(
                        ApiError.NO_META,
                        ApiError.PRIVATE_DOCUMENTS_WITHOUT_PUBLIC_KEYS,
                        ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_COUNT
                    )
                )
            )
        )

        val apiSpecGeneratedJson = openApi3RenderJson(spec).encodePrettily()
        val expectedJson = File("$resDir/api-spec.json").readText()

        assertEquals(expectedJson.trim(), apiSpecGeneratedJson.trim())
    }
}
