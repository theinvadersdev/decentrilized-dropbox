package uk.co.outchain

import io.vertx.ext.web.client.HttpRequest
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.junit5.VertxTestContext

fun <T> HttpRequest<*>.codec(responseCodec: BodyCodec<T>): HttpRequest<T> {
    return this.`as`(responseCodec)
}

inline fun VertxTestContext.junitVertxCatch(crossinline work: () -> Unit) {
    try {
        work()
    } catch (e: Throwable) {
        failNow(e)
    }
}