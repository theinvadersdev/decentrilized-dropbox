package uk.co.outchain

import uk.co.outchain.crypt.CryptVerticle
import uk.co.outchain.crypt.decryptFile
import uk.co.outchain.crypt.decryptMasterKey
import uk.co.outchain.crypt.loadPrivateKeyCipher
import uk.co.outchain.http.ApiError
import uk.co.outchain.http.HttpServerVerticle
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import uk.co.outchain.http.StatusCodes
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import okhttp3.*
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.junit.jupiter.api.*
import java.io.File
import io.vertx.core.DeploymentOptions
import java.nio.file.Files
import java.nio.file.Paths

@ExtendWith(VertxExtension::class)
class ApiTest {
    private lateinit var httpVerticle: HttpServerVerticle
    private lateinit var cryptVerticle: CryptVerticle

    companion object {
        private const val RES_DIR = "src/test/resources/uk/co/outchain"
        private const val UPLOAD_DOCUMENT_ENDPOINT = "http://0.0.0.0:8080/upload-document"
        private const val GET_DOCUMENT_BY_ID_ENDPOINT = "http://0.0.0.0:8080/get-document-by-id"

        private val config =
            json {
                obj(
                    Config.MASTER_KEYS_PATH to "$RES_DIR/master_keys",
                    Config.ENCRYPTED_DOCUMENT_ROOT_PATH to "$RES_DIR/encrypted_documents",
                    Config.PUBLIC_DOCUMENT_ROOT_PATH to "$RES_DIR/public_documents",
                    Config.TO_ENCRYPT_DOCUMENT_ROOT_PATH to "$RES_DIR/to_encrypt_documents"
                )
            }
    }

    @AfterEach
    fun cleanUp() {
        FileUtils.deleteDirectory(File(config.getString(Config.MASTER_KEYS_PATH)))
        FileUtils.deleteDirectory(File(config.getString(Config.ENCRYPTED_DOCUMENT_ROOT_PATH)))
        FileUtils.deleteDirectory(File(config.getString(Config.PUBLIC_DOCUMENT_ROOT_PATH)))
        FileUtils.deleteDirectory(File(config.getString(Config.TO_ENCRYPT_DOCUMENT_ROOT_PATH)))
        FileUtils.deleteDirectory(File("$RES_DIR/tmp"))
    }

    @BeforeEach
    fun setUp() {
        File(config.getString(Config.MASTER_KEYS_PATH)).mkdir()
        File(config.getString(Config.ENCRYPTED_DOCUMENT_ROOT_PATH)).mkdir()
        File(config.getString(Config.PUBLIC_DOCUMENT_ROOT_PATH)).mkdir()
        File(config.getString(Config.TO_ENCRYPT_DOCUMENT_ROOT_PATH)).mkdir()
        File("$RES_DIR/tmp").mkdir()
    }

    private fun assertApiError(excepted: ApiError, actual: JsonObject) {
        assertEquals(excepted.code, actual.getInteger("error_code"))
        assertEquals(excepted.message, actual.getString("dev_hint"))
    }

    @BeforeEach
    fun deploy_verticle(vertx: Vertx, testContext: VertxTestContext) {
        val options = DeploymentOptions().setConfig(config)

        httpVerticle = HttpServerVerticle()
        cryptVerticle = CryptVerticle()

        vertx.deployVerticle(httpVerticle, options, testContext.succeeding { _ ->
            vertx.deployVerticle(cryptVerticle, options, testContext.succeeding { _ ->
                testContext.completeNow()
            })
        })
    }

    private fun requestBody(meta: String? = null, keys: List<String>, documents: List<String>): RequestBody {
        val requestBodyBuilder = MultipartBody.Builder()
            .setType(MultipartBody.FORM)

        if (meta != null) {
            val body = RequestBody.create(MediaType.parse("application/json"), File("$RES_DIR/uploaddocument/$meta"))
            requestBodyBuilder.addFormDataPart("meta", "meta.json", body)
        }

        for (key in keys) {
            val body = RequestBody.create(null, File("$RES_DIR/$key"))
            requestBodyBuilder.addFormDataPart("public_key[]", key, body)
        }

        for (doc in documents) {
            val body = RequestBody.create(null, File("$RES_DIR/$doc"))
            requestBodyBuilder.addFormDataPart("document[]", doc, body)
        }

        return requestBodyBuilder.build()
    }

    private fun doUploadHandlerTest(testContext: VertxTestContext, meta: String? = null,
                                    keys: List<String> = listOf(), documents: List<String> = listOf(),
                                    assertions: (Response) -> Unit) {
        testContext.junitVertxCatch {
            val client = OkHttpClient()
            val requestBody = requestBody(meta, keys, documents)

            val request = Request.Builder()
                .url(UPLOAD_DOCUMENT_ENDPOINT)
                .post(requestBody)
                .build()

            val response = client.newCall(request).execute()

            assertions(response)
        }
    }

    @Test
    fun `test upload handler failed without meta`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            keys = listOf("user-1-public_key"),
            documents = listOf("bender.png")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.NO_META, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed without private documents without public keys`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-private-documents-without-public-keys.json",
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.PRIVATE_DOCUMENTS_WITHOUT_PUBLIC_KEYS, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed public keys does not match with meta count`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_COUNT, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed documents does not match with meta count`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.DOCUMENTS_NOT_MATCH_WITH_META_COUNT, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed documents does not match with meta filename`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-repeat-documents-filenames.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.DOCUMENTS_NOT_MATCH_WITH_META_FILENAME, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed public keys does not match with meta filename`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-repeat-public-keys-filenames.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_FILENAME, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed public keys does not match with meta identity`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-repeat-public-keys-identities.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_IDENTITY, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed documents does not match with meta sh256`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-repeat-documents-sha256.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.DOCUMENTS_NOT_MATCH_WITH_META_SHA256, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed meta json invalid`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-invalid.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.META_INVALID_JSON, JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed invalid key`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-invalid-key.json",
            keys = listOf("user-1-private_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!
            val error = JsonObject(body.string())

            assertEquals(ApiError.FailToScheduleDocument.spec.code, error.getInteger("error_code"))
            assertEquals("Failed to schedule document: java.security.InvalidKeyException: IOException: algid parse error, not a sequence", error.getString("dev_hint"))

            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed meta json missed required fields`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "meta-with-missed-fields.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg", "bender_2.png")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            val missedFields = listOf(
                "public_keys[0].file_name",
                "public_keys[1].identity",
                "documents[0].file_name",
                "documents[1].sha256",
                "documents[2].public"
            )

            assertApiError(ApiError.MetaMissedRequiredFieldInJson(missedFields), JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed meta json can't be match with uploaded document`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.png")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.DocumentsNotMatchWithMeta("bender.jpg"), JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler failed meta json can't be match with uploaded public keys`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-1-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.BAD_REQUEST_400, response.code())
            assertNotNull(response.body())

            val body = response.body()!!

            assertApiError(ApiError.PublicKeysNotMatchWithMeta("user-2-public_key"), JsonObject(body.string()))
            testContext.completeNow()
        }

    @Test
    fun `test upload handler success files were uploaded`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.OK_200, response.code())
            assertNotNull(response.body())

            val body = JsonObject(response.body()!!.string())
            val documents = body.getJsonArray("documents")

            assertNotNull(documents)
            assertEquals(2, documents.size())

            val privateDocument = documents.getJsonObject(0)
            val publicDocument = documents.getJsonObject(1)

            assertEquals("finished", publicDocument.getString("status"))
            assertTrue(File(publicDocument.getString("debug_server_path")).exists())

            assertNull(publicDocument.getValue("master_keys"))

            val masterKeys = privateDocument.getJsonArray("master_keys")

            assertNotNull(masterKeys)
            assertEquals(2, masterKeys.size())
            assertTrue(File(masterKeys.getJsonObject(0).getString("debug_server_path")).exists())
            assertTrue(File(masterKeys.getJsonObject(1).getString("debug_server_path")).exists())

            testContext.completeNow()
        }

    @Test
    fun `test upload handler success files were scheduled`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            assertEquals(StatusCodes.OK_200, response.code())
            assertNotNull(response.body())

            val body = JsonObject(response.body()!!.string())
            val documents = body.getJsonArray("documents")

            assertNotNull(documents)
            assertEquals(2, documents.size())

            val document1 = documents.getJsonObject(0)
            val document2 = documents.getJsonObject(1)

            assertNotNull(document1.getString("id"))
            assertNotNull(document2.getString("id"))

            assertEquals("scheduled", document1.getString("status"))
            assertEquals("6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8", document1.getString("sha256sum"))
            assertEquals("bender.png", document1.getString("file_name"))

            assertEquals(false, document1.getBoolean("is_public"))
            assertNull(document1.getValue("eth_block_hash"))

            val masterKeys = document1.getJsonArray("master_keys")
            assertNotNull(masterKeys)

            assertEquals(2, masterKeys.size())

            val path1 = masterKeys.getJsonObject(0).getString("debug_server_path")
            val path2 = masterKeys.getJsonObject(1).getString("debug_server_path")

            val url1 = "http://localhost:8080/master-keys/" + FilenameUtils.getName(path1)
            val url2 = "http://localhost:8080/master-keys/" + FilenameUtils.getName(path2)

            assertEquals("user-1", masterKeys.getJsonObject(0).getString("identity"))
            assertEquals(url1, masterKeys.getJsonObject(0).getString("absolute_url"))

            assertEquals("user-2", masterKeys.getJsonObject(1).getString("identity"))
            assertEquals(url2, masterKeys.getJsonObject(1).getString("absolute_url"))

            assertEquals("finished", document2.getString("status"))
            assertEquals("21318e1b39ddddf4763c58563cd0d5bab9a2b9615a7be2a984cf1e2e43aaca92", document2.getString("sha256sum"))
            assertEquals("bender.jpg", document2.getString("file_name"))
            assertEquals("http://localhost:8080/public-documents/${document2.getString("id")}-bender.jpg", document2.getString("absolute_url"))
            assertEquals(true, document2.getBoolean("is_public"))
            assertNull(document2.getValue("crypt_algorithm"))
            assertNull(document2.getValue("master_keys"))
            assertNull(document2.getValue("eth_block_hash"))

            testContext.completeNow()
        }

    @Test
    fun `test getDocumentByIdHandler`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            val body = JsonObject(response.body()!!.string())
            var documentBody: JsonObject = body.getJsonArray("documents").getJsonObject(0)
            val id = documentBody.getString("id")
            var status = ""
            var counter = 0

            while (status != "finished") {
                assertTrue(counter < 5, "Exceed limit")
                counter += 1

                Thread.sleep(500)
                val client = OkHttpClient()

                val request = Request.Builder()
                    .url("$GET_DOCUMENT_BY_ID_ENDPOINT/$id")
                    .build()

                val documentResponse = client.newCall(request).execute()
                assertEquals(StatusCodes.OK_200, documentResponse.code())
                documentBody = JsonObject(documentResponse.body()!!.string())

                status = documentBody.getString("status")
            }

            assertEquals("AES/CBC/PKCS5PADDING", documentBody.getString("crypt_algorithm"))
            assertEquals("bender.png", documentBody.getString("file_name"))
            assertEquals("http://localhost:8080/encrypted-documents/$id-bender.png.aes_cbc_pkcs5padding.enc", documentBody.getString("absolute_url"))

            testContext.completeNow()
        }

    @Test
    fun `test upload handler encrypted documents can be decrypted`(vertx: Vertx, testContext: VertxTestContext) =
        doUploadHandlerTest(testContext,
            meta = "valid-meta.json",
            keys = listOf("user-1-public_key", "user-2-public_key"),
            documents = listOf("bender.png", "bender.jpg")
        ) { response ->
            val body = JsonObject(response.body()!!.string())
            var documentBody: JsonObject = body.getJsonArray("documents").getJsonObject(0)
            val id = documentBody.getString("id")
            var status = ""
            var counter = 0

            while (status != "finished") {
                assertTrue(counter < 5, "Exceed limit")
                counter += 1

                Thread.sleep(500)
                val client = OkHttpClient()

                val request = Request.Builder()
                    .url("$GET_DOCUMENT_BY_ID_ENDPOINT/$id")
                    .build()

                val documentResponse = client.newCall(request).execute()
                assertEquals(StatusCodes.OK_200, documentResponse.code())
                documentBody = JsonObject(documentResponse.body()!!.string())

                status = documentBody.getString("status")
            }

            val decCipher = loadPrivateKeyCipher("$RES_DIR/user-1-private_key")
            val masterKeyPath = documentBody.getJsonArray("master_keys").getJsonObject(0).getString("debug_server_path")
            val encodedMasterKey = Files.readAllBytes(Paths.get(masterKeyPath))
            val masterKey = decryptMasterKey(decCipher, encodedMasterKey)
            val encryptedDocumentPath = documentBody.getString("debug_server_path")

            val decryptedContent: ByteArray = decryptFile(masterKey, encryptedDocumentPath) {
                it.readBytes()
            }

            val content = Files.readAllBytes(Paths.get("$RES_DIR/bender.png"))
            assertArrayEquals(content, decryptedContent)

            testContext.completeNow()
        }
}
