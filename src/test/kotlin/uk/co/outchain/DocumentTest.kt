package uk.co.outchain

import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.ExtendWith
import org.opentest4j.AssertionFailedError
import uk.co.outchain.crypt.*
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.*

@ExtendWith(VertxExtension::class)
class DocumentTest {
    private lateinit var cryptVerticle: CryptVerticle

    companion object {
        private const val resDir = "src/test/resources/uk/co/outchain"

        private val config =
            json {
                obj(
                    Config.MASTER_KEYS_PATH to "$resDir/master_keys",
                    Config.ENCRYPTED_DOCUMENT_ROOT_PATH to "$resDir/encrypted_documents",
                    Config.PUBLIC_DOCUMENT_ROOT_PATH to "$resDir/public_documents"
                )
            }

        private val masterKeyRootPath = config.getString(Config.MASTER_KEYS_PATH)
        private val documentsRootPath = config.getString(Config.ENCRYPTED_DOCUMENT_ROOT_PATH)
        private val publicDocumentsRootPath = config.getString(Config.PUBLIC_DOCUMENT_ROOT_PATH)

        @AfterAll
        @JvmStatic
        fun cleanUp() {
            FileUtils.deleteDirectory(File(config.getString(Config.MASTER_KEYS_PATH)))
            FileUtils.deleteDirectory(File(config.getString(Config.ENCRYPTED_DOCUMENT_ROOT_PATH)))
            FileUtils.deleteDirectory(File(config.getString(Config.PUBLIC_DOCUMENT_ROOT_PATH)))
            FileUtils.deleteDirectory(File("$resDir/tmp"))
        }

        @BeforeAll
        @JvmStatic
        fun setUp() {
            File(config.getString(Config.MASTER_KEYS_PATH)).mkdir()
            File(config.getString(Config.ENCRYPTED_DOCUMENT_ROOT_PATH)).mkdir()
            File(config.getString(Config.PUBLIC_DOCUMENT_ROOT_PATH)).mkdir()
            File("$resDir/tmp").mkdir()
        }
    }

    @BeforeEach
    fun deploy_verticle(vertx: Vertx, testContext: VertxTestContext) {
        cryptVerticle = CryptVerticle()
        vertx.deployVerticle(cryptVerticle, testContext.succeeding { _ ->
            val query = json { obj() }
            cryptVerticle.mongoClient.remove("documents", query) {
                testContext.completeNow()
            }
        })
    }

    fun rxVertx(vertx: Vertx): io.vertx.reactivex.core.Vertx {
        return io.vertx.reactivex.core.Vertx(vertx)
    }

    @Test
    fun `test success add insertion job`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(
                InsertDocumentPublicKey(
                    identity = "user-1",
                    path = "$resDir/user-1-public_key"
                ),
                InsertDocumentPublicKey(
                    identity = "user-2",
                    path = "$resDir/user-2-public_key"
                )
            ),
            sourceFile = "$resDir/bender.png",
            fileName = "bender.png",
            sha256sum = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8",
            isPublic = false
        )

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeBy(
                onSuccess = { documentScheduleResult ->
                    testContext.junitVertxCatch {
                        assertNotNull(documentScheduleResult.job)

                        val payload = documentScheduleResult.payload!!

                        assertNotNull(payload.getString("_id", null))
                        assertEquals("6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8", payload.getString("sha256sum"))
                        assertEquals("bender.png", payload.getString("filename"))

                        val id = payload.getString("_id")

                        val masterKey1 = payload.getJsonArray("master_keys").getJsonObject(0)
                        val masterKey2 = payload.getJsonArray("master_keys").getJsonObject(1)

                        val keyPath1 = masterKey1.getString("key_path")
                        val keyPath2 = masterKey2.getString("key_path")

                        assertEquals("user-1", masterKey1.getString("identity"))
                        assertEquals("$masterKeyRootPath/$id-user-1.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey", keyPath1)
                        assertEquals("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", masterKey1.getString("crypt_algorithm"))

                        assertEquals("user-2", masterKey2.getString("identity"))
                        assertEquals("$masterKeyRootPath/$id-user-2.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey", keyPath2)
                        assertEquals("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", masterKey2.getString("crypt_algorithm"))

                        assertTrue(File(keyPath1).exists())
                        assertTrue(File(keyPath2).exists())
                    }

                    val query = json {
                        obj("_id" to documentScheduleResult.payload!!.getString("_id", null))
                    }

                    cryptVerticle.mongoClient.find("documents", query) { res ->
                        testContext.junitVertxCatch {
                            assertTrue(res.succeeded())
                            val items = res.result()
                            assertEquals(1, items.size)

                            val item = items[0]

                            assertEquals("6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8", item.getString("sha256sum"))
                            assertEquals("scheduled", item.getString("status"))
                            assertEquals("bender.png", item.getString("filename"))

                            val id = item.getString("_id")

                            val user1MasterKeyPath = "$masterKeyRootPath/$id-user-1.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey"
                            val user2MasterKeyPath = "$masterKeyRootPath/$id-user-2.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey"

                            val masterKey1 = item.getJsonArray("master_keys").getJsonObject(0)
                            val masterKey2 = item.getJsonArray("master_keys").getJsonObject(1)

                            assertEquals("user-1", masterKey1.getString("identity"))
                            assertEquals(user1MasterKeyPath, masterKey1.getString("key_path"))
                            assertEquals("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", masterKey1.getString("crypt_algorithm"))

                            assertEquals("user-2", masterKey2.getString("identity"))
                            assertEquals(user2MasterKeyPath, masterKey2.getString("key_path"))
                            assertEquals("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", masterKey2.getString("crypt_algorithm"))

                            val masterKey = Base64.getDecoder().decode(item.getString("processing_master_key"))

                            val decCipher1 = loadPrivateKeyCipher("$resDir/user-1-private_key")
                            val decCipher2 = loadPrivateKeyCipher("$resDir/user-2-private_key")

                            val encodedMasterKey1 = Files.readAllBytes(Paths.get(user1MasterKeyPath))
                            val encodedMasterKey2 = Files.readAllBytes(Paths.get(user2MasterKeyPath))

                            assertArrayEquals(masterKey, decryptMasterKey(decCipher1, encodedMasterKey1))
                            assertArrayEquals(masterKey, decryptMasterKey(decCipher2, encodedMasterKey2))

                            testContext.completeNow()
                        }
                    }
                },
                onError = {
                    testContext.failNow(it)
                }
            )
    }

    @Test
    fun `test success upload public document`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(),
            sourceFile = "$resDir/tmp/bender.png",
            fileName = "bender.png",
            sha256sum = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8",
            isPublic = true
        )

        Files.copy(Paths.get("$resDir/bender.png"), Paths.get("$resDir/tmp/bender.png"), StandardCopyOption.REPLACE_EXISTING)

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeBy(
                onSuccess = { documentScheduleResult ->
                    testContext.junitVertxCatch {
                        assertNull(documentScheduleResult.job)

                        val payload = documentScheduleResult.payload!!

                        assertNotNull(payload.getString("_id", null))
                        assertEquals("6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8", payload.getString("sha256sum"))
                        assertEquals("bender.png", payload.getString("filename"))
                        assertEquals(true, payload.getBoolean("is_public"))
                    }

                    val query = json {
                        obj("_id" to documentScheduleResult.payload!!.getString("_id", null))
                    }

                    cryptVerticle.mongoClient.find("documents", query) { res ->
                        testContext.junitVertxCatch {
                            assertTrue(res.succeeded())
                            val items = res.result()
                            assertEquals(1, items.size)

                            val item = items[0]

                            assertEquals("6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8", item.getString("sha256sum"))
                            assertEquals("finished", item.getString("status"))
                            assertEquals("bender.png", item.getString("filename"))
                            assertEquals(true, item.getBoolean("is_public"))

                            val id = item.getString("_id")
                            assertEquals("$publicDocumentsRootPath/$id-bender.png", item.getString("document_path"))
                            assertTrue(File(item.getString("document_path")).exists())

                            testContext.completeNow()
                        }
                    }
                },
                onError = {
                    testContext.failNow(it)
                }
            )
    }

    @Test
    fun `test success add insertion job finished`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(
                InsertDocumentPublicKey(
                    identity = "user-1",
                    path = "$resDir/user-1-public_key"
                ),
                InsertDocumentPublicKey(
                    identity = "user-2",
                    path = "$resDir/user-2-public_key"
                )
            ),
            sourceFile = "$resDir/tmp/bender.png",
            fileName = "bender.png",
            sha256sum = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8",
            isPublic = false
        )

        Files.copy(Paths.get("$resDir/bender.png"), Paths.get("$resDir/tmp/bender.png"), StandardCopyOption.REPLACE_EXISTING)

        var step = 0

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.single())
            .toFlowable()
            .concatMap { insertDocumentResult ->
                assertNotNull(insertDocumentResult.job)
                insertDocumentResult.job!!
                    .map {
                        Pair(insertDocumentResult, it)
                    }
            }
            .concatMap { (insertDocumentResult, documentStatus) ->
                val query = json {
                    obj("_id" to insertDocumentResult.payload!!.getString("_id"))
                }

                cryptVerticle.mongoClient
                    .rxFind("documents", query)
                    .map { Pair(it, documentStatus) }
                    .toFlowable()
            }
            .subscribeBy(
                onNext = { (items, documentStatus) ->
                    step += 1

                    testContext.junitVertxCatch {
                        assertEquals(1, items.size)

                        val item = items[0]
                        val id = item.getString("_id")

                        assertEquals("6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8", item.getString("sha256sum"))
                        assertEquals("bender.png", item.getString("filename"))
                        assertEquals("AES/CBC/PKCS5PADDING", item.getString("crypt_algorithm"))
                        assertEquals("$documentsRootPath/$id-bender.png.aes_cbc_pkcs5padding.enc", item.getString("encrypted_document_path"))

                        val masterKey1 = item.getJsonArray("master_keys").getJsonObject(0)
                        val masterKey2 = item.getJsonArray("master_keys").getJsonObject(1)

                        assertEquals("user-1", masterKey1.getString("identity"))
                        assertEquals("$masterKeyRootPath/$id-user-1.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey", masterKey1.getString("key_path"))
                        assertEquals("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", masterKey1.getString("crypt_algorithm"))

                        assertEquals("user-2", masterKey2.getString("identity"))
                        assertEquals("$masterKeyRootPath/$id-user-2.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey", masterKey2.getString("key_path"))
                        assertEquals("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", masterKey2.getString("crypt_algorithm"))

                        when (step) {
                            1 -> {
                                assertEquals(DocumentStatus.PROCESSING, documentStatus)
                                assertEquals(DocumentStatus.PROCESSING.key, item.getString("status"))
                            }
                            2 -> {
                                assertEquals(DocumentStatus.FINISHED, documentStatus)
                                assertEquals(DocumentStatus.FINISHED.key, item.getString("status"))

                                assertNull(item.getString("processing_master_key", null))
                                assertNull(item.getString("file_to_encrypt", null))
                                assertFalse(File(input.sourceFile).exists())

                                val decCipher = loadPrivateKeyCipher("$resDir/user-1-private_key")
                                val masterKeyPath = item.getJsonArray("master_keys").getJsonObject(0).getString("key_path")
                                val encodedMasterKey = Files.readAllBytes(Paths.get(masterKeyPath))
                                val masterKey = decryptMasterKey(decCipher, encodedMasterKey)
                                val encryptedDocumentPath = item.getString("encrypted_document_path")

                                val decryptedContent: ByteArray = decryptFile(masterKey, encryptedDocumentPath) {
                                    it.readBytes()
                                }

                                val content = Files.readAllBytes(Paths.get("$resDir/bender.png"))
                                assertArrayEquals(content, decryptedContent)

                                testContext.completeNow()
                            }
                        }
                    }
                },
                onComplete = {
                    testContext.junitVertxCatch {
                        assertEquals(2, step)
                    }
                },
                onError = {
                    testContext.failNow(it)
                }
            )
    }

    @Test
    @Disabled
    fun `test success add insertion job failed due to interruption process`(vertx: Vertx, testContext: VertxTestContext) {
        testContext.completeNow()
    }

    @Test
    fun `test fail add insertion job due to wrong sha256sum`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(
                InsertDocumentPublicKey(
                    identity = "user-1",
                    path = "$resDir/user-1-public_key"
                ),
                InsertDocumentPublicKey(
                    identity = "user-2",
                    path = "$resDir/user-2-public_key"
                )
            ),
            sourceFile = "$resDir/bender.png",
            fileName = "bender.png",
            sha256sum = "21318e1b39ddddf4763c58563cd0d5bab9a2b9615a7be2a984cf1e2e43aaca92",
            isPublic = false
        )

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeBy(
                onSuccess = {
                    testContext.failNow(AssertionFailedError("Expected an error but was success"))
                },
                onError = {
                    testContext.junitVertxCatch {
                        assertEquals("sha256sum does not match the file uploaded on the server", it.message)
                        testContext.completeNow()
                    }
                }
            )
    }

    @Test
    fun `test fail add insertion job due to missing keys`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(),
            sourceFile = "$resDir/bender.png",
            fileName = "bender.png",
            sha256sum = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8",
            isPublic = false
        )

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeBy(
                onSuccess = {
                    testContext.failNow(AssertionFailedError("Expected an error but was success"))
                },
                onError = {
                    testContext.junitVertxCatch {
                        assertEquals("at least one public key must be provided", it.message)
                        testContext.completeNow()
                    }
                }
            )
    }

    @Test
    fun `test fail add insertion job due to missed file to encrypt`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(
                InsertDocumentPublicKey(
                    identity = "user-1",
                    path = "$resDir/user-1-public_key"
                )
            ),
            sourceFile = "$resDir/missed_bender.png",
            fileName = "bender.png",
            sha256sum = "???",
            isPublic = false
        )

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeBy(
                onSuccess = {
                    testContext.failNow(AssertionFailedError("Expected an error but was success"))
                },
                onError = {
                    testContext.junitVertxCatch {
                        assertEquals("cannot find document to encrypt", it.message)
                        testContext.completeNow()
                    }
                }
            )
    }

    @Test
    fun `test fail add insertion job due to missed file to exception`(vertx: Vertx, testContext: VertxTestContext) {
        val input = InsertDocumentInput(
            config = config,
            publicKeys = listOf(
                InsertDocumentPublicKey(
                    identity = "user-1",
                    path = "$resDir/missed-public_key"
                )
            ),
            sourceFile = "$resDir/bender.png",
            fileName = "bender.png",
            sha256sum = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8",
            isPublic = false
        )

        insertDocument(rxVertx(vertx), cryptVerticle.mongoClient, input)
            .subscribeBy(
                onSuccess = {
                    testContext.failNow(AssertionFailedError("Expected an error but was success"))
                },
                onError = {
                    testContext.junitVertxCatch {
                        assertTrue(it is java.nio.file.NoSuchFileException)
                        assertEquals("$resDir/missed-public_key", it.message)
                        testContext.completeNow()
                    }
                }
            )
    }
}
