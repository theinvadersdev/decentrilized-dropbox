import requests
import hashlib

def sha256(file_name):
    hash_sha256 = hashlib.sha256()

    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)

    return hash_sha256.hexdigest()

bender_png_sha256 = sha256("bender.png")
bender_jpg_sha256 = sha256("bender.jpg")

url = "http://localhost:8080/upload-document"
meta_payload = \
  """
    {
      "public_keys": [
        {
          "file_name": "user-1-public_key",
          "identity": "user-1"
        },
        {
          "file_name": "user-2-public_key",
          "identity": "user-2"
        }
      ],
      "documents": [
        {
          "file_name": "bender.png",
          "sha256": "%s",
          "public": false
        },
        {
          "file_name": "bender.jpg",
          "sha256": "%s",
          "public": true
        }
      ]
    }
  """ % (bender_png_sha256, bender_jpg_sha256)

files = [
  (
    "meta", (
      "meta.json",
      meta_payload,
      "application/json"
    )
  ),
  (
    "public_key[]", (
      "user-1-public_key",
      open("user-1-public_key", "rb")
    )
  ),
  (
    "public_key[]", (
      "user-2-public_key",
      open("user-2-public_key", "rb")
    )
  ),
  (
    "document[]", (
      "bender.png",
      open("bender.png", "rb")
    )
  ),
  (
    "document[]", (
      "bender.jpg",
      open("bender.jpg", "rb")
    )
  )
]

response = requests.post(url, files=files)
print(response.content)
