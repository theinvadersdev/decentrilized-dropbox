const node1Endpoint = "http://127.0.0.1:60000";
const node2Endpoint = "http://127.0.0.1:60010";

$("document").ready(function() {
    Rx.Observable
        .fromEvent($("#node1-upload-form"), "submit")
        .map((event) => {
            event.preventDefault();
            return 0;
        })
        .subscribe((response) => {
            alert("TODO");
        });

    function loadItems(endpoint, $list) {
        $.getJSON(endpoint + "/get-documents", function (data) {
            const items = data.map(x => `<li><a href="/decrypt/?url=${x.absolute_url}">${x.file_name}</a></li>`);
            $list.html(items.join("\n"));
        });
    }

    loadItems(node1Endpoint, $("#node1-list-items"));
    loadItems(node2Endpoint, $("#node2-list-items"));
});
