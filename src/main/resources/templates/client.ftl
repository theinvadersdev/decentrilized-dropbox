<!DOCTYPE html>
<html>
<head>
    <title>Клиент</title>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:300,400,700" rel="stylesheet">
</head>
<body>

<div style="float: left; width: 50%">
    <h2>Файлы для Node 1</h2>

    <ul id="node1-list-items">
    </ul>
</div>

<div style="float: left; width: 50%">
    <h2>Файлы для Node 2</h2>

    <ul id="node2-list-items">
    </ul>
</div>

<div style="clear: both"></div>
<hr>

<div style="float: left; width: 50%">
    <h2>Добавить файлы на Node 1</h2>
    <form enctype="multipart/form-data" id="node1-upload-form">
        <div>
            <input type="file" multiple>
        </div>
        <br>
        <input type="submit" value="Загрузить">
    </form>
</div>

<div style="float: left; width: 50%">
    <h2>Добавить файлы на Node 2</h2>
    <form enctype="multipart/form-data" id="node2-upload-form">
        <div>
            <input type="file" multiple>
        </div>
        <br>
        <input type="submit" value="Загрузить">
    </form>
</div>

<div style="clear: both"></div>

<script src="https://npmcdn.com/@reactivex/rxjs@5.0.0-alpha.8/dist/global/Rx.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

<script src="static/main.js" type="module"></script>

</body>
</html>
