package uk.co.outchain.http

object StatusCodes {
    const val OK_200 = 200
    const val CREATED_201 = 201
    const val NO_CONTENT = 204

    const val SEE_OTHER_303 = 303

    const val BAD_REQUEST_400 = 400
    const val UNAUTHORIZED_401 = 401
    const val FORBIDDEN_403 = 403
    const val NOT_FOUND_404 = 404

    const val INTERNAL_SERVER_ERROR_500 = 500
    const val BAD_GATEWAY_502 = 502
}
