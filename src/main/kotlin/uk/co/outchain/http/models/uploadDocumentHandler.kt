package uk.co.outchain.http.models

import uk.co.outchain.http.PropertySpec

data class UploadRequestBody(
    @PropertySpec(
        required = true,
        obj = MetaRequestBody::class
    )
    val meta: MetaRequestBody,

    @PropertySpec(
        required = true,
        arrayFormat = "binary",
        arrayType = "string",
        fieldName = "public_key[]",
        description = "Публичные ключи"
    )
    val public_keys: List<String>,

    @PropertySpec(
        required = true,
        arrayFormat = "binary",
        arrayType = "string",
        fieldName = "document[]",
        description = "Документы"
    )
    val documents: List<String>
)

data class MetaRequestBody(
    @PropertySpec(
        required = true,
        description = "Мета информация для публичных ключей",
        array = PublicKeyMetaRequestBody::class
    )
    val public_keys: List<PublicKeyMetaRequestBody>?,

    @PropertySpec(
        required = true,
        description = "Мета информация для документов",
        array = DocumentMetaRequestBody::class
    )
    val documents: List<DocumentMetaRequestBody>?
) : RequiredField {
    override fun collectMissedFields(missedFields: MutableList<String>, prefix: String?) {
        collectMissedField(missedFields, public_keys, prefix, "public_keys")
        collectMissedField(missedFields, documents, prefix, "documents")
        collectMissedFieldsInList(missedFields, public_keys, "public_keys")
        collectMissedFieldsInList(missedFields, documents, "documents")
    }
}

data class MetaValidated(
    val publicKeys: List<PublicKeyMetaRequestBody>,
    val documents: List<DocumentMetaRequestBody>
)

data class PublicKeyMetaRequestBody(
    @PropertySpec(
        required = true,
        example = "user-1-public_key"
    )
    val file_name: String?,

    @PropertySpec(
        required = true,
        example = "user-1"
    )
    val identity: String?
) : RequiredField {
    override fun collectMissedFields(missedFields: MutableList<String>, prefix: String?) {
        collectMissedField(missedFields, file_name, prefix, "file_name")
        collectMissedField(missedFields, identity, prefix, "identity")
    }
}

data class DocumentMetaRequestBody(
    @PropertySpec(
        required = true,
        example = "bender.png"
    )
    val file_name: String?,

    @PropertySpec(
        required = true,
        format = "sha256",
        example = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8"
    )
    val sha256: String?,

    @PropertySpec(
        required = true
    )
    val public: Boolean?
) : RequiredField {
    override fun collectMissedFields(missedFields: MutableList<String>, prefix: String?) {
        collectMissedField(missedFields, file_name, prefix, "file_name")
        collectMissedField(missedFields, sha256, prefix, "sha256")
        collectMissedField(missedFields, public, prefix, "public")
    }
}
