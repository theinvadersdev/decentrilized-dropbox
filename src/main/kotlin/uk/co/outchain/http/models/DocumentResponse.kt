package uk.co.outchain.http.models

import uk.co.outchain.http.PropertySpec

data class DocumentResponse(
    @PropertySpec(
        required = true,
        example = "5bac7c3f792f2a480e7d169c"
    )
    val id: String,

    @PropertySpec(
        required = true,
        type = "string",
        enum = [
            "scheduled - Задача добавлена в очередь для выполнения",
            "processing - Документ шифруется",
            "finished - Документ успешно зашифрован"
        ]
    )
    val status: String,

    @PropertySpec(
        required = true,
        format = "sha256",
        example = "6a69ebc81ba6c0d756696cd59d110e6fb5557c8341edabab1365ea51a38cdbb8",
        description = "SHA-256 хеш сумма документа"
    )
    val sha256sum: String,

    @PropertySpec(
        required = true,
        example = "document.pdf",
        description = "Имя файла"
    )
    val file_name: String,

    @PropertySpec(
        required = true,
        description = "Является ли документ публичным"
    )
    val is_public: Boolean,

    @PropertySpec(
        description = "Алгоритм шифрования документа",
        type = "string",
        enum = [
            "AES/CBC/PKCS5PADDING"
        ]
    )
    val crypt_algorithm: String? = null,

    @PropertySpec(
        description = "Абсолютный url до шифрованного документа",
        example = "http://localhost:8080/encrypted-documents/5bac7c3f792f2a480e7d169c-bender.png.aes_cbc_pkcs5padding.enc"
    )
    val absolute_url: String? = null,

    @PropertySpec(
        description = "Путь до документа на сервере (только в дебаг режиме)"
    )
    val debug_server_path: String? = null,

    @PropertySpec(
        description = "Мастер ключи для дешифровки, null, если документ публичный",
        array = MasterKeyResponse::class
    )
    val master_keys: List<MasterKeyResponse>? = null
)

data class MasterKeyResponse(
    @PropertySpec(
        required = true,
        description = "Владелец ключа"
    )
    val identity: String,

    @PropertySpec(
        required = true,
        description = "Абсолютный url до ключа",
        example = "http://localhost:8080/encrypted-documents/5bac7c3f792f2a480e7d169c-user-1.bender.png.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey"
    )
    val absolute_url: String,

    @PropertySpec(
        required = true,
        description = "Алгоритм шифрования ключа",
        enum = [
            "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"
        ]
    )
    val crypt_algorithm: String,

    @PropertySpec(
        description = "Путь до ключа на сервере (только в дебаг режиме)"
    )
    val debug_server_path: String? = null
)
