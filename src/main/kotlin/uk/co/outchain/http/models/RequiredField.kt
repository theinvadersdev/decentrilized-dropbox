package uk.co.outchain.http.models

interface RequiredField {
    fun collectMissedFields(missedFields: MutableList<String>, prefix: String? = null)
}

fun collectMissedField(missedFields: MutableList<String>, value: Any?, prefix: String?, name: String) {
    if (value == null)
        missedFields.add(prefix + name)
}

fun <T : RequiredField> collectMissedFieldsInList(missedFields: MutableList<String>, items: List<T>?, name: String) {
    if (items == null)
        return

    items.forEachIndexed { index, item ->
        item.collectMissedFields(missedFields, "$name[$index].")
    }
}
