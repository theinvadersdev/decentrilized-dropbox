package uk.co.outchain.http

import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext

data class MethodHandler(
    val spec: ApiSpecMethod,
    val handler: (context: RoutingContext) -> Unit
) {
    fun addToRouter(router: Router) {
        when (spec.method) {
            ApiMethod.GET -> router.get(spec.url).handler(handler)
            ApiMethod.POST -> router.post(spec.url).handler(handler)
            ApiMethod.PUT -> router.put(spec.url).handler(handler)
            ApiMethod.PATCH -> router.patch(spec.url).handler(handler)
            ApiMethod.DELETE -> router.delete(spec.url).handler(handler)
        }
    }
}
