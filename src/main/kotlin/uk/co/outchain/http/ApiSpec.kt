package uk.co.outchain.http

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import java.lang.UnsupportedOperationException
import java.nio.file.Paths
import kotlin.collections.ArrayList
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
annotation class PropertySpec(
    val fieldName: String = "",
    val example: String = "",
    val type: String = "",
    val format: String = "",
    val description: String = "",
    val enum: Array<String> = [],
    val array: KClass<*> = Unit::class,
    val arrayType: String = "",
    val arrayFormat: String = "",
    val arrayDescription: String = "",
    val obj: KClass<*> = Unit::class,
    val required: Boolean = false
)

data class ApiSpec(
    val description: String,
    val version: String,
    val title: String,
    val servers: List<ApiSpecServer>,
    val methods: MutableList<ApiSpecMethod>
)

data class ApiSpecServer(
    val url: String,
    val description: String = ""
)

enum class ApiMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}

data class ApiSpecMethod(
    val url: String,
    val method: ApiMethod,
    val tags: List<String> = listOf(),
    val summary: String = "",
    val description: String = "",
    val parameters: List<SpecParameter> = listOf(),
    val responses: List<ApiSpecResponse> = listOf(),
    val requestBody: ApiSpecContent? = null,
    val samples: List<ApiSpecCodeSample> = listOf(),
    val errors: List<ApiError> = listOf()
)

data class ApiSpecResponse(
    val statusCode: Int,
    val description: String,
    val content: ApiSpecContent? = null
)

data class SpecParameter(
    val name: String,
    val type: String,
    val description: String = "",
    val format: String = "",
    val in_: String = "path",
    val required: Boolean = true
)

val defaultApiSpecResponse400 = ApiSpecResponse(
    statusCode = StatusCodes.BAD_REQUEST_400,
    description = "Ошибка запроса",
    content = ApiSpecContent(
        contentType = "application/json",
        schema = ApiError::class
    )
)

data class ApiSpecContent(
    val contentType: String,
    val schema: KClass<*>
)

data class ApiSpecCodeSample(
    val lang: String,
    val samplePath: String
)

private fun loadSample(path: String): String {
    val classloader = Thread.currentThread().contextClassLoader
    return String(classloader.getResourceAsStream(Paths.get("samples", path).toString()).readBytes())
}

fun openApi3RenderJson(apiSpec: ApiSpec): JsonObject {
    val schemas = JsonObject()
    val requestBodies = JsonObject()

    for (method in apiSpec.methods) {
        for (response in method.responses) {
            val cls = response.content?.schema ?: continue
            schemas.put(cls.simpleName, getSchema(cls))
        }

        val cls = method.requestBody?.schema ?: continue
        requestBodies.put(cls.simpleName, getSchema(cls))
    }

    var mainDescription = apiSpec.description
    val allErrors = apiSpec.methods.flatMap { it.errors }

    if (allErrors.isNotEmpty()) {
        mainDescription += "\n# Все ошибки\n\n"

        mainDescription += "### Тело ошибки\n\n"
        mainDescription += loadSample("default_error_body.json")
            .trim()
            .prependIndent("    ")

        mainDescription += "\n"

        val errorsForTag = allErrors
            .asSequence()
            .distinctBy { it.code }
            .groupBy { it.tag }

        for (entry in errorsForTag) {
            val tag = entry.key
            val errors = entry.value

            mainDescription += "## $tag\n\n"
            mainDescription += generateErrors(errors)
        }
    }

    return json {
        obj(
            "openapi" to "3.0.0",
            "info" to obj(
                "description" to mainDescription,
                "version" to apiSpec.version,
                "title" to apiSpec.title
            ),
            "servers" to apiSpec.servers.map {
                obj(
                    "url" to it.url,
                    "description" to it.description
                )
            },
            "paths" to generatePaths(apiSpec.methods),
            "components" to obj(
                "schemas" to schemas,
                "requestBodies" to requestBodies
            )
        )
    }
}

private fun generatePaths(methods: List<ApiSpecMethod>): Map<String, Any?> =
    json {
        methods.asSequence().groupBy { it.url }
            .map { entry ->
                val url = entry.key.replace(Regex(":(.+)"), "{$1}")
                val specs = entry.value

                url to specs.map { spec ->
                    val methodJson = obj(
                        "tags" to spec.tags,
                        "summary" to spec.summary
                    )

                    var description = spec.description

                    if (spec.requestBody != null) {
                        methodJson.put("requestBody", getContent("requestBodies", spec.requestBody))
                    }

                    if (spec.parameters.isNotEmpty()) {
                        methodJson.put("parameters", generateParameters(spec.parameters))
                    }

                    if (spec.responses.isNotEmpty()) {
                        methodJson.put("responses", generateResponse(spec.responses))
                    }

                    if (spec.errors.isNotEmpty()) {
                        description += "\n\n### Ошибки\n\n"
                        description += generateErrors(spec.errors)
                    }

                    if (spec.samples.isNotEmpty()) {
                        methodJson.put("x-code-samples", generateSamples(spec.samples))
                    }

                    methodJson.put("description", description)
                    spec.method.name.toLowerCase() to methodJson
                }.toMap()
            }.toMap()
    }

private fun generateParameters(parameters: List<SpecParameter>): List<JsonObject> =
    parameters.map { parameterSpec ->
        json {
            obj(
                "name" to parameterSpec.name,
                "type" to parameterSpec.type,
                "in" to parameterSpec.in_,
                "description" to parameterSpec.description,
                "required" to parameterSpec.required,
                "schema" to obj(
                    "type" to parameterSpec.type,
                    "format" to parameterSpec.format
                )
            )
        }
    }

private fun generateResponse(responses: List<ApiSpecResponse>): Map<String, Any?> =
    json {
        responses.asSequence().map {
            val responseMap = JsonObject()
            responseMap.put("description", it.description)

            if (it.content != null) {
                responseMap.mergeIn(getContent("schemas", it.content))
            }

            it.statusCode.toString() to responseMap
        }
            .toMap()
    }

private fun generateErrors(errors: List<ApiError>): String {
    var description = ""

    description += "| Код | Описание |\n"
    description += "| --- | -------- |\n"

    for (error in errors) {
        description += "| ${error.code} | ${error.specMessage} |\n"
    }

    return description
}

private fun generateSamples(samples: List<ApiSpecCodeSample>): List<JsonObject> =
    samples.map {
        json {
            obj(
                "lang" to it.lang,
                "source" to loadSample(it.samplePath)
            )
        }
    }

private fun getContent(refLocation: String, content: ApiSpecContent): JsonObject =
    json {
        obj(
            "content" to obj(
                content.contentType to obj(
                    "schema" to obj(
                        "\$ref" to "#/components/$refLocation/${content.schema.simpleName}"
                    )
                )
            )
        )
    }

private fun getSchema(cls: KClass<*>): JsonObject {
    val schema = JsonObject()
    val properties = JsonObject()
    val required = ArrayList<String>()

    for (member in cls.memberProperties) {
        val property = member.annotations.firstOrNull { it.annotationClass == PropertySpec::class } as? PropertySpec
            ?: continue

        val memberName = if (property.fieldName.isNotBlank()) {
            property.fieldName
        } else {
            member.name
        }

        val type = if (property.type.isNotBlank()) {
            property.type
        } else if (property.obj != Unit::class) {
            "object"
        } else {
            when (member.returnType.classifier) {
                String::class -> "string"
                Boolean::class -> "boolean"
                Int::class -> "integer"
                Long::class -> "integer"
                List::class -> "array"
                else -> throw UnsupportedOperationException("Unspecified type for field ${member.name} of type ${member.returnType} for $cls")
            }
        }

        if (property.required) {
            required.add(memberName)
        }

        val propertiesBody = json {
            obj(
                "type" to type,
                "format" to property.format,
                "description" to property.description
            )
        }

        if (property.example.isNotBlank()) {
            propertiesBody.put("example", property.example)
        }

        if (type == "array" && property.array != Unit::class) {
            propertiesBody.put("items", getSchema(property.array))
        } else if (type == "array") {
            propertiesBody.put("items", json {
                obj(
                    "type" to property.arrayType,
                    "format" to property.arrayFormat,
                    "description" to property.arrayDescription
                )
            })
        }

        if (property.obj != Unit::class) {
            propertiesBody.mergeIn(getSchema(property.obj))
        }

        if (!property.enum.isEmpty()) {
            val enumPairs = property.enum.toList().map {
                val split = it.split("-")
                if (split.size == 2) {
                    split[0].trim() to split[1].trim()
                } else {
                    it to null
                }
            }
            propertiesBody.put(
                "enum",
                enumPairs.map { it.first }
            )

            val desc = enumPairs
                .asSequence()
                .filter { it.second != null }
                .map { (value, description) ->
                    "- `$value` - $description"
                }
                .joinToString("\n")
                .trim()

            if (desc.isNotBlank()) {
                propertiesBody.put("description", desc)
            }
        }

        properties.put(memberName, propertiesBody)
    }

    schema.put("type", "object")
    schema.put("properties", properties)
    schema.put("required", required)

    return schema
}
