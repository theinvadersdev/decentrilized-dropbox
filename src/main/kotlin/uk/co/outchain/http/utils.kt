package uk.co.outchain.http

import io.vertx.core.json.JsonObject
import io.vertx.ext.web.handler.impl.HttpStatusException
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.reactivex.ext.web.RoutingContext

fun jsonResponseEnd(context: RoutingContext, body: JsonObject, statusCode: Int) {
    context.response()
        .putHeader("Content-Type", "application/json")
        .putHeader("Access-Control-Allow-Origin", "*")
        .setStatusCode(statusCode)
        .end(body.encode())
}

fun jsonResponseEnd(context: RoutingContext, body: String, statusCode: Int) {
    context.response()
        .putHeader("Content-Type", "application/json")
        .putHeader("Access-Control-Allow-Origin", "*")
        .setStatusCode(statusCode)
        .end(body)
}

open class ApiError(
    @PropertySpec(
        fieldName = "error_code",
        required = true,
        description = "Код ошибки"
    )
    val code: Int,

    @PropertySpec(
        fieldName = "dev_hint",
        required = true,
        description = "Описание что пошло не так для разработчиков апи",
        example = "there is no meta data"
    )
    val message: String = "",

    val specMessage: String = "",
    val statusCode: Int = -1,

    val tag: String = ""
) {
    companion object {
        val NO_META = ApiError(
            code = 0,
            tag = "Документы",
            message = "there is no meta data",
            specMessage = "Не была предоставлена мета информация для документов и ключей",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val PRIVATE_DOCUMENTS_WITHOUT_PUBLIC_KEYS = ApiError(
            code = 1,
            tag = "Документы",
            message = "you have private documents and no public keys for encryption",
            specMessage = "Попытка загрузить приватные документы без ключей шифрования",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val PUBLIC_KEYS_NOT_MATCH_WITH_META_COUNT = ApiError(
            code = 2,
            tag = "Документы",
            message = "count of public keys in meta data less or greater then uploaded",
            specMessage = "Количество публичных ключей в мета информации не соответствует количеству загруженных ключей",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val DOCUMENTS_NOT_MATCH_WITH_META_COUNT = ApiError(
            code = 3,
            tag = "Документы",
            message = "count of documents in meta data less or greater then uploaded",
            specMessage = "Количество документов в мета информации не соответствует количеству загруженных докуметов",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val PUBLIC_KEYS_NOT_MATCH_WITH_META_FILENAME = ApiError(
            code = 4,
            tag = "Документы",
            message = "public keys not match with meta filename",
            specMessage = "Публичные ключи в мета информации не соответствуют загруженным по полю file_name",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val DOCUMENTS_NOT_MATCH_WITH_META_FILENAME = ApiError(
            code = 5,
            tag = "Документы",
            message = "documents not match with meta filename",
            specMessage = "Документы в мета информации не соответствуют загруженным по полю file_name",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val PUBLIC_KEYS_NOT_MATCH_WITH_META_IDENTITY = ApiError(
            code = 6,
            tag = "Документы",
            message = "public keys not match with meta identity",
            specMessage = "Публичные ключи в мета информации не соответствуют загруженным по полю identity",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val DOCUMENTS_NOT_MATCH_WITH_META_SHA256 = ApiError(
            code = 7,
            tag = "Документы",
            message = "documents not match with meta sha256",
            specMessage = "Документы в мета информации не соответствуют загруженным по sha256",
            statusCode = StatusCodes.BAD_REQUEST_400
        )

        val META_INVALID_JSON = ApiError(
            code = 8,
            tag = "Документы",
            message = "uploaded json meta is invalid",
            specMessage = "Не валидный json у загруженного мета файла",
            statusCode = StatusCodes.BAD_REQUEST_400
        )
    }

    class MetaMissedRequiredFieldInJson(requiredFieldAreMissed: List<String>) :
        ApiError(
            code = 9,
            tag = "Документы",
            message = "missed required fields: ${requiredFieldAreMissed.joinToString(", ")}",
            specMessage = "Не заполнены обязательные поля в мета информации",
            statusCode = StatusCodes.BAD_REQUEST_400
        )
    {
        companion object {
            val spec = MetaMissedRequiredFieldInJson(listOf())
        }
    }

    class PublicKeysNotMatchWithMeta(val fileName: String) :
        ApiError(
            code = 10,
            tag = "Документы",
            message = "cannot match public key meta data '$fileName' with public key",
            specMessage = "Не удалось сопоставить публичные ключи с мета инфорацией.",
            statusCode = StatusCodes.BAD_REQUEST_400
        )
    {
        companion object {
            val spec = PublicKeysNotMatchWithMeta("")
        }
    }

    class DocumentsNotMatchWithMeta(val fileName: String) :
        ApiError(
            code = 11,
            tag = "Документы",
            message = "cannot match document meta data '$fileName' with document",
            specMessage = "Не удалось сопоставить документы с мета инфорацией.",
            statusCode = StatusCodes.BAD_REQUEST_400
        )
    {
        companion object {
            val spec = DocumentsNotMatchWithMeta("")
        }
    }

    class FailToScheduleDocument(throwable: Throwable) :
        ApiError(
            code = 12,
            tag = "Документы",
            message = throwable.message ?: "failed to schedule document",
            specMessage = "Не удалось добавить задачу в пул",
            statusCode = StatusCodes.BAD_REQUEST_400
        )
    {
        companion object {
            val spec = FailToScheduleDocument(Throwable())
        }
    }

    class FailToRetrieveDocument(throwable: Throwable) :
        ApiError(
            code = 13,
            tag = "Документы",
            message = throwable.message ?: "failed to retrieve document",
            specMessage = "Не удалось найти документ",
            statusCode = StatusCodes.BAD_REQUEST_400
        )
    {
        companion object {
            val spec = FailToRetrieveDocument(Throwable())
        }
    }
}

fun apiFailure(context: RoutingContext, error: ApiError, statusCode: Int): Throwable {
    val body = json {
        obj(
            "error_code" to error.code,
            "dev_hint" to error.message
        )
    }
    jsonResponseEnd(context, body, statusCode)
    return HttpStatusException(statusCode)
}

fun apiFailure(context: RoutingContext, throwable: Throwable, statusCode: Int = StatusCodes.INTERNAL_SERVER_ERROR_500): Throwable =
    apiFailure(context,
        ApiError(code = -1, message = throwable.message ?: ""),
        statusCode
    )

fun apiFailure(context: RoutingContext, error: ApiError): Throwable =
    apiFailure(context, error, error.statusCode)
