package uk.co.outchain.http

import io.vertx.core.Future
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext
import io.vertx.reactivex.ext.web.templ.FreeMarkerTemplateEngine
import org.slf4j.LoggerFactory
import java.nio.file.Paths

class ClientVerticle : AbstractVerticle() {
    companion object {
        private val LOGGER = LoggerFactory.getLogger(ClientVerticle::class.java)
        private const val CONFIG_HTTP_CLIENT_PORT = "http.client.port"
    }

    private val templateEngine = FreeMarkerTemplateEngine.create()

    override fun start(startFuture: Future<Void>) {
        val router = Router.router(vertx)

        with(router) {
            get("/").handler(::indexHandler)
            get("/static/main.js").handler(::mainJsHandler)
        }

        val portNumber = config().getInteger(CONFIG_HTTP_CLIENT_PORT, 8081)
        vertx.createHttpServer()
            .requestHandler(router::accept)
            .listen(portNumber) {
                LOGGER.info("Successfully connected to localhost:$portNumber")
                startFuture.complete()
            }
    }

    private fun indexHandler(context: RoutingContext) {
        templateEngine.render(context, "templates", "/client.ftl") { ar ->
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", "text/html")
                context.response().end(ar.result())
            } else {
                context.fail(ar.cause())
            }
        }
    }

    private fun mainJsHandler(context: RoutingContext) {
        val classloader = Thread.currentThread().contextClassLoader
        val text = String(classloader.getResourceAsStream(Paths.get("static", "main.js").toString()).readBytes())

        context.response()
            .putHeader("Content-Type", "application/javascript")
            .end(text)
    }
}
