package uk.co.outchain.http

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import uk.co.outchain.Config
import uk.co.outchain.crypt.CryptActions
import uk.co.outchain.crypt.CryptVerticle
import uk.co.outchain.crypt.CryptReplyErrorCodes
import uk.co.outchain.http.models.*
import io.vertx.core.Future
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.eventbus.DeliveryOptions
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext
import io.vertx.reactivex.ext.web.handler.BodyHandler
import io.vertx.reactivex.ext.web.templ.FreeMarkerTemplateEngine
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Paths

class HttpServerVerticle : AbstractVerticle() {
    companion object {
        private val LOGGER = LoggerFactory.getLogger(HttpServerVerticle::class.java)
        private const val CONFIG_HTTP_SERVER_PORT = "http.server.port"
    }

    private lateinit var apiSpecJson: String
    private lateinit var queue: String
    private val templateEngine = FreeMarkerTemplateEngine.create()
    private val gson = Gson()

    override fun start(startFuture: Future<Void>) {
        queue = config().getString(CryptVerticle.CONFIG_CRYPT_QUEUE, "crypt.queue")
        val router = Router.router(vertx)

        with(router) {
            val uploadDir = config().getString(Config.TMP_ROOT_PATH, "tmp")
            post().handler(BodyHandler.create().setUploadsDirectory(uploadDir))

            getDocumentByHashHandler.addToRouter(router)
            getDocumentByIdHandler.addToRouter(router)
            uploadDocumentHandler.addToRouter(router)
            getDocuments.addToRouter(router)

            get("/spec").handler(::docHandler)
            get("/spec.json").handler(::specJsonHandler)
        }

        initDoc()

        val portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 8080)
        vertx.createHttpServer()
            .requestHandler(router::accept)
            .listen(portNumber) {
                LOGGER.info("Successfully connected to localhost:$portNumber")
                startFuture.complete()
            }
    }

    private fun initDoc() {
        val spec = ApiSpec(
            description = "Сервис для загрузки и шифрования документов",
            version = "1.0.0",
            title = "Decentralized Dropbox",
            servers = listOf(
                ApiSpecServer(
                    url = "http://35.227.57.194/",
                    description = "Тестовый сервер размещенный на google cloud platform."
                )
            ),
            methods = mutableListOf(
                uploadDocumentHandler.spec,
                getDocumentByIdHandler.spec,
                getDocumentByHashHandler.spec,
                getDocuments.spec
            )
        )

        apiSpecJson = openApi3RenderJson(spec).encodePrettily()
    }

    private fun docHandler(context: RoutingContext) {
        templateEngine.render(context, "templates", "/doc.ftl") { ar ->
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", "text/html")
                context.response().end(ar.result())
            } else {
                context.fail(ar.cause())
            }
        }
    }

    private fun specJsonHandler(context: RoutingContext) {
        context.response().putHeader("Content-Type", "application/json")
        context.response().end(apiSpecJson)
    }

    private val getDocumentByHashHandler = MethodHandler(
        spec = ApiSpecMethod(
            url = "/get-document-by-sha256/:hash",
            method = ApiMethod.GET,
            tags = listOf("document"),
            summary = "Получение документа по хеш сумме",
            parameters = listOf(
                SpecParameter(
                    name = "hash",
                    type = "string",
                    format = "sha256",
                    description = "SHA-256 хеш сумма документа"
                )
            ),
            responses = listOf(
                ApiSpecResponse(
                    statusCode = 200,
                    description = "Успешеная операция",
                    content = ApiSpecContent(
                        contentType = "application/json",
                        schema = DocumentResponse::class
                    )
                ),
                defaultApiSpecResponse400
            ),
            errors = listOf(
                ApiError.FailToRetrieveDocument.spec
            )
        )
    ) { context ->
        val hash = context.request().getParam("hash")
        val options = DeliveryOptions().addHeader("action", CryptActions.RETRIEVE_DOCUMENT_DATA_FROM_HASH)
        val request = json { obj("document-sha256" to hash) }

        vertx.eventBus().send<JsonObject>(queue, request, options) { reply ->
            if (reply.succeeded()) {
                val body = json {
                    obj(
                        "success" to true,
                        // TODO: Body
                        "payload" to reply.result().body()
                    )
                }

                jsonResponseEnd(context, body, StatusCodes.OK_200)
            } else {
                handleReplyError(context, reply.cause())
            }
        }
    }

    private val getDocumentByIdHandler = MethodHandler(
        spec = ApiSpecMethod(
            url = "/get-document-by-id/:id",
            method = ApiMethod.GET,
            tags = listOf("document"),
            summary = "Получение документа по id",
            parameters = listOf(
                SpecParameter(
                    name = "id",
                    type = "string",
                    description = "Уникальный идентификатор документа"
                )
            ),
            responses = listOf(
                ApiSpecResponse(
                    statusCode = 200,
                    description = "Успешеная операция",
                    content = ApiSpecContent(
                        contentType = "application/json",
                        schema = DocumentResponse::class
                    )
                ),
                defaultApiSpecResponse400
            ),
            errors = listOf(
                ApiError.FailToRetrieveDocument.spec
            )
        )
    ) { context ->
        val id = context.request().getParam("id")
        val options = DeliveryOptions().addHeader("action", CryptActions.RETRIEVE_DOCUMENT_DATA_BY_ID)
        val request = json { obj("id" to id) }

        vertx.eventBus().send<JsonObject>(queue, request, options) { reply ->
            if (reply.succeeded()) {
                val payload = serializeDocument(reply.result().body())
                jsonResponseEnd(context, gson.toJson(payload), StatusCodes.OK_200)
            } else {
                handleReplyError(context, reply.cause())
            }
        }
    }

    private val getDocuments = MethodHandler(
        spec = ApiSpecMethod(
            url = "/get-documents",
            method = ApiMethod.GET,
            tags = listOf("document"),
            summary = "Получение всех зашифрованных документов",
            parameters = listOf(
                SpecParameter(
                    name = "id",
                    type = "string",
                    description = "Уникальный идентификатор документа"
                )
            ),
            responses = listOf(
                ApiSpecResponse(
                    statusCode = 200,
                    description = "Успешеная операция",
                    content = ApiSpecContent(
                        contentType = "application/json",
                        schema = DocumentResponse::class
                    )
                ),
                defaultApiSpecResponse400
            ),
            errors = listOf(
                ApiError.FailToRetrieveDocument.spec
            )
        )
    ) { context ->
        val options = DeliveryOptions().addHeader("action", CryptActions.RETRIEVE_ALL_DOCUMENTS)
        val request = json { obj() }

        vertx.eventBus().send<JsonArray>(queue, request, options) { reply ->
            if (reply.succeeded()) {
//                reply.result().body()
                val payload = serializeDocuments(reply.result().body())
                jsonResponseEnd(context, gson.toJson(payload), StatusCodes.OK_200)
            } else {
                handleReplyError(context, reply.cause())
            }
        }
    }

    private val uploadDocumentHandler = MethodHandler(
        spec = ApiSpecMethod(
            url = "/upload-document",
            method = ApiMethod.POST,
            tags = listOf("document"),
            summary = "Загрузка документов",
            description = """
                Метод загружает документы на центральный сервер и шифрует приватные документы.

                ### Способ шифрования

                Генерируется случайны мастер ключ и данный мастер ключ шифруется отправленными
                публичными ключами, таким образом получается несколько мастер ключей - для каждого
                участника свой ключ, который он в будущем может расшифровать своим приватным ключем.

                Ключи генерируются алгоритмом RSA, формат для публичного ключа - X509,
                формат приватного ключа PKCS8.

                Мастер ключ шифруется при помощи ассиметричного шифрования RSA (RSA/ECB/OAEPWithSHA-256AndMGF1Padding).

                Документ шифруется при помощи блочного шифрования AES (AES/CBC/PKCS5PADDING).
            """.trimIndent(),

            requestBody = ApiSpecContent(
                contentType = "multipart/form-data",
                schema = UploadRequestBody::class
            ),
            responses = listOf(
                ApiSpecResponse(
                    statusCode = 200,
                    description = "Успешеная операция",
                    content = ApiSpecContent(
                        contentType = "application/json",
                        schema = DocumentResponse::class
                    )
                ),
                defaultApiSpecResponse400
            ),
            samples = listOf(
                ApiSpecCodeSample("meta.json", "meta.json"),
                ApiSpecCodeSample("python", "python.py")
            ),
            errors = listOf(
                ApiError.NO_META,
                ApiError.PRIVATE_DOCUMENTS_WITHOUT_PUBLIC_KEYS,
                ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_COUNT,
                ApiError.DOCUMENTS_NOT_MATCH_WITH_META_COUNT,
                ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_FILENAME,
                ApiError.DOCUMENTS_NOT_MATCH_WITH_META_FILENAME,
                ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_IDENTITY,
                ApiError.DOCUMENTS_NOT_MATCH_WITH_META_SHA256,
                ApiError.META_INVALID_JSON,
                ApiError.MetaMissedRequiredFieldInJson.spec,
                ApiError.PublicKeysNotMatchWithMeta.spec,
                ApiError.DocumentsNotMatchWithMeta.spec,
                ApiError.FailToScheduleDocument.spec
            )
        )
    ) { context ->
        context.response().isChunked = true

        val meta = getUploadDocumentHandlerMetaData(context)
        val documentsPayloads = meta.documents
            .map { documentMeta ->
                val file = context.fileUploads().firstOrNull { it.fileName() == documentMeta.file_name }
                    ?: throw apiFailure(context, ApiError.DocumentsNotMatchWithMeta(documentMeta.file_name!!))

                json {
                    obj(
                        "file_name" to documentMeta.file_name,
                        "uploaded_path" to file.uploadedFileName(),
                        "sha256sum" to documentMeta.sha256,
                        "public" to documentMeta.public
                    )
                }
            }

        val publicKeysPayloads = meta.publicKeys
            .map { publicKeyMeta ->
                val file = context.fileUploads().firstOrNull { it.fileName() == publicKeyMeta.file_name }
                    ?: throw apiFailure(context, ApiError.PublicKeysNotMatchWithMeta(publicKeyMeta.file_name!!))

                json {
                    obj(
                        "file_name" to publicKeyMeta.file_name,
                        "uploaded_path" to file.uploadedFileName(),
                        "identity" to publicKeyMeta.identity
                    )
                }
            }

        val options = DeliveryOptions().addHeader("action", CryptActions.SCHEDULE_CRYPT_DOCUMENT)
        val request = json {
            obj(
                "documents" to documentsPayloads,
                "public_keys" to publicKeysPayloads
            )
        }

        vertx.eventBus().send<JsonObject>(queue, request, options) { reply ->
            if (reply.succeeded()) {
                val replyBody = reply.result().body()
                val responseBody = mapOf(
                    "documents" to replyBody.getJsonArray("documents").list.map {
                        serializeDocument(it as JsonObject)
                    }
                )
                jsonResponseEnd(context, gson.toJson(responseBody), StatusCodes.OK_200)
            } else {
                handleReplyError(context, reply.cause())
            }
        }
    }

    private fun handleReplyError(context: RoutingContext, cause: Throwable) {
        if (cause is ReplyException) {
            when (cause.failureCode()) {
                CryptReplyErrorCodes.FAIL_TO_RETRIEVE ->
                    apiFailure(context, ApiError.FailToRetrieveDocument(cause))

                CryptReplyErrorCodes.FAIL_TO_SCHEDULE ->
                    apiFailure(context, ApiError.FailToScheduleDocument(cause))

                else -> apiFailure(context, cause)
            }
        } else {
            apiFailure(context, cause)
        }
    }

    private fun serializeDocuments(documents: JsonArray): List<DocumentResponse> {
        return documents
            .map { serializeDocument(it as JsonObject) }
    }

    private fun serializeDocument(document: JsonObject): DocumentResponse {
        val isPublic = document.getBoolean("is_public")

        val masterKeys = document.getJsonArray("master_keys")
            ?.map { masterKey ->
                val masterKeyJson = masterKey as JsonObject

                val urlRoot = Config.masterKeyRootUrl(config())
                val fileName = Paths.get(masterKeyJson.getString("key_path")).fileName.toString()

                MasterKeyResponse(
                    identity = masterKeyJson.getString("identity"),
                    absolute_url = "$urlRoot/$fileName",
                    crypt_algorithm = masterKeyJson.getString("crypt_algorithm"),
                    debug_server_path = if (Config.includeDebugInfo(config())) {
                        masterKeyJson.getString("key_path")
                    } else {
                        null
                    }
                )
            }

        val absoluteUrl = if (isPublic && document.getString("status") == "finished") {
            val urlRoot = Config.publicDocumentUrl(config())
            val fileName = Paths.get(document.getString("document_path")).fileName.toString()
            "$urlRoot/$fileName"
        } else if (document.getString("status") == "finished") {
            val urlRoot = Config.encryptedDocumentRootUrl(config())

            val fileName = Paths.get(document.getString("encrypted_document_path")).fileName.toString()
            "$urlRoot/$fileName"
        } else {
            null
        }

        val debugServerPath = if (Config.includeDebugInfo(config())) {
            if (isPublic) {
                document.getString("document_path")
            } else {
                document.getString("encrypted_document_path")
            }
        } else {
            null
        }

        return DocumentResponse(
            id = document.getString("_id"),
            status = document.getString("status"),
            sha256sum = document.getString("sha256sum"),
            file_name = document.getString("filename"),
            is_public = isPublic,
            crypt_algorithm = document.getString("crypt_algorithm"),
            master_keys = masterKeys,
            absolute_url = absoluteUrl,
            debug_server_path = debugServerPath
        )
    }

    private fun getUploadDocumentHandlerMetaData(context: RoutingContext): MetaValidated {
        val metaFile = context.fileUploads().firstOrNull { it.name() == "meta" }
            ?: throw apiFailure(context, ApiError.NO_META)

        val jsonData = File(metaFile.uploadedFileName()).readText()

        val meta = try {
            Gson().fromJson(jsonData, MetaRequestBody::class.java)
        } catch (e: JsonSyntaxException) {
            throw apiFailure(context, ApiError.META_INVALID_JSON)
        }

        val documents = context.fileUploads().filter { it.name() == "document[]" }
        val publicKeys = context.fileUploads().filter { it.name() == "public_key[]" }

        val missedFields = ArrayList<String>()
        meta.collectMissedFields(missedFields)

        if (missedFields.isNotEmpty())
            throw apiFailure(context, ApiError.MetaMissedRequiredFieldInJson(missedFields))

        if (meta.public_keys?.size != publicKeys.size)
            throw apiFailure(context, ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_COUNT)

        if (meta.documents?.size != documents.size)
            throw apiFailure(context, ApiError.DOCUMENTS_NOT_MATCH_WITH_META_COUNT)

        // Имя файлов должны быть уникальный
        if (meta.public_keys.size != meta.public_keys.distinctBy { it.file_name }.size)
            throw apiFailure(context, ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_FILENAME)

        if (meta.documents.size != meta.documents.distinctBy { it.file_name }.size)
            throw apiFailure(context, ApiError.DOCUMENTS_NOT_MATCH_WITH_META_FILENAME)

        if (meta.public_keys.size != meta.public_keys.distinctBy { it.identity }.size)
            throw apiFailure(context, ApiError.PUBLIC_KEYS_NOT_MATCH_WITH_META_IDENTITY)

        if (meta.documents.size != meta.documents.distinctBy { it.sha256 }.size)
            throw apiFailure(context, ApiError.DOCUMENTS_NOT_MATCH_WITH_META_SHA256)

        if (publicKeys.isEmpty() && meta.documents.any { !it.public!! })
            throw apiFailure(context, ApiError.PRIVATE_DOCUMENTS_WITHOUT_PUBLIC_KEYS)

        return MetaValidated(
            documents = meta.documents,
            publicKeys = meta.public_keys
        )
    }
}
