package uk.co.outchain

import io.vertx.core.json.JsonObject

object Config {
    const val INCLUDE_DEBUG_INFO = "include_debug_info"

    const val MASTER_KEYS_PATH = "storage.master_key_paths"
    const val ENCRYPTED_DOCUMENT_ROOT_PATH = "storage.encrypted_document_root_path"
    const val TO_ENCRYPT_DOCUMENT_ROOT_PATH = "storage.to_encrypt_document_root_path"
    const val PUBLIC_DOCUMENT_ROOT_PATH = "storage.public_document_root_path"
    const val TMP_ROOT_PATH = "storage.tmp"

    const val ENCRYPTED_DOCUMENT_ROOT_URL = "storage_url.encrypted_document_root_url"
    const val PUBLIC_DOCUMENT_URL = "storage_url.public_document_url"
    const val MASTER_KEY_ROOT_URL = "storage_url.master_key_root_url"

    fun includeDebugInfo(config: JsonObject): Boolean =
        config.getBoolean(INCLUDE_DEBUG_INFO) ?: true

    fun masterKeysPath(config: JsonObject): String =
        config.getString(MASTER_KEYS_PATH) ?: "uploads/master-keys"

    fun encryptedDocumentRootPath(config: JsonObject): String =
        config.getString(ENCRYPTED_DOCUMENT_ROOT_PATH) ?: "uploads/encrypted"

    fun toEncryptDocumentRootPath(config: JsonObject): String =
        config.getString(TO_ENCRYPT_DOCUMENT_ROOT_PATH) ?: "uploads/to-encrypt"

    fun publicDocumentRootPath(config: JsonObject): String =
        config.getString(PUBLIC_DOCUMENT_ROOT_PATH) ?: "uploads/public-documents"

    fun encryptedDocumentRootUrl(config: JsonObject): String =
        config.getString(ENCRYPTED_DOCUMENT_ROOT_URL) ?: "http://localhost:8080/encrypted-documents"

    fun publicDocumentUrl(config: JsonObject): String =
        config.getString(PUBLIC_DOCUMENT_URL) ?: "http://localhost:8080/public-documents"

    fun masterKeyRootUrl(config: JsonObject): String =
        config.getString(MASTER_KEY_ROOT_URL) ?: "http://localhost:8080/master-keys"
}
