package uk.co.outchain.crypt

import com.google.gson.Gson
import uk.co.outchain.Config
import io.reactivex.Flowable
import io.reactivex.rxkotlin.subscribeBy
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.JsonObject
import io.vertx.kotlin.core.json.array
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.core.RxHelper
import io.vertx.reactivex.core.Vertx
import io.vertx.reactivex.core.eventbus.Message
import io.vertx.reactivex.ext.mongo.MongoClient
import org.slf4j.LoggerFactory
import java.nio.file.Paths

class CryptVerticle : AbstractVerticle() {
    companion object {
        private val LOGGER = LoggerFactory.getLogger(CryptVerticle::class.java)

        const val CONFIG_CRYPT_QUEUE = "crypt.queue"
    }

    lateinit var mongoClient: MongoClient
        private set

    val gson = Gson()

    override fun start(startFuture: Future<Void>) {
        val config = Vertx.currentContext().config()

        val uri = config.getString("mongodb.uri")!!
        val db = config.getString("mongodb.database")!!

        val mongoConfig = json {
            obj(
                "connection_string" to uri,
                "db_name" to db
            )
        }

        mongoClient = MongoClient.createNonShared(vertx, mongoConfig)

        val queue = config().getString(CONFIG_CRYPT_QUEUE, "crypt.queue")
        vertx.eventBus().consumer(queue, ::onMessage)
        startFuture.complete()
    }

    private fun onMessage(message: Message<JsonObject>) {
        if (!message.headers().contains("action")) {
            LOGGER.error("No action header specified for message with headers ${message.headers()} and body ${message.body().encodePrettily()}")
            message.fail(CryptReplyErrorCodes.NO_ACTION_SPECIFIED, "No action header specified")
            return
        }

        val action = message.headers().get("action")

        when (action) {
            CryptActions.SCHEDULE_CRYPT_DOCUMENT -> scheduleCryptDocument(message)
            CryptActions.RETRIEVE_DOCUMENT_DATA_BY_ID -> getDocumentById(message)
            CryptActions.RETRIEVE_ALL_DOCUMENTS -> getAllDocuments(message)
            else -> message.fail(CryptReplyErrorCodes.BAD_ACTION, "Bad action: $action")
        }
    }

    private fun scheduleCryptDocument(message: Message<JsonObject>) {
        val includeDebugInfo = Config.includeDebugInfo(config())
        val toEncryptDocumentsPath = Config.toEncryptDocumentRootPath(config())
        val publicDocumentsPath = Config.publicDocumentRootPath(config())

        val documents = message.body().getJsonArray("documents")!!
        val publicKeys = message.body().getJsonArray("public_keys")!!

        // Копирование документов в папку для шифрования либо в папку публичных документов.
        Flowable.fromIterable(documents)
            .concatMapSingle {
                val document = it as JsonObject

                val isPublic = document.getBoolean("public")
                val source = Paths.get(document.getString("uploaded_path"))
                val fileName = source.fileName.toString()

                val dest = if (isPublic) {
                    Paths.get(publicDocumentsPath, fileName).toAbsolutePath()
                } else {
                    Paths.get(toEncryptDocumentsPath, fileName).toAbsolutePath()
                }

                vertx.fileSystem().rxMove(source.toAbsolutePath().toString(), dest.toString())
                    .toSingle {
                        val payload = HashMap<String, Any>()

                        if (includeDebugInfo) {
                            payload["debug_server_path"] = dest.toString()
                        }

                        payload
                    }
                    .flatMap { _ ->
                        val input = InsertDocumentInput(
                            config = config(),
                            publicKeys = publicKeys.map { key ->
                                val jsonKey = key as JsonObject
                                InsertDocumentPublicKey(
                                    identity = jsonKey.getString("identity"),
                                    path = Paths.get(jsonKey.getString("uploaded_path"))
                                        .toAbsolutePath()
                                        .toString()
                                )
                            },
                            sourceFile = dest.toString(),
                            fileName = document.getString("file_name"),
                            sha256sum = document.getString("sha256sum"),
                            isPublic = isPublic
                        )

                        insertDocument(vertx, mongoClient, input)
                            .map { insertResult ->
                                if (insertResult.job != null) {
                                    insertResult.job
                                        .subscribeOn(RxHelper.scheduler(vertx))
                                        .observeOn(RxHelper.scheduler(vertx))
                                        .subscribeBy(
                                            onError = { throwable ->
                                                LOGGER.error("Error occur while processing encryption job", throwable)
                                            }
                                        )
                                }

                                insertResult.payload
                            }
                    }
            }
            .toList()
            .subscribeBy(
                onSuccess = { it ->
                    message.reply(json { obj("documents" to it) })
                },
                onError = {
                    if (it is DocumentError) {
                        LOGGER.error(it.message, it)
                        message.fail(CryptReplyErrorCodes.FAIL_TO_SCHEDULE, it.message)
                    } else {
                        LOGGER.error("Failed to schedule document", it)
                        message.fail(CryptReplyErrorCodes.FAIL_TO_SCHEDULE, "Failed to schedule document: ${it.message}")
                    }
                }
            )
    }

    private fun getDocumentById(message: Message<JsonObject>) {
        val id = message.body().getString("id")

        if (id == null) {
            LOGGER.error("id is null")
            message.fail(CryptReplyErrorCodes.FAIL_TO_RETRIEVE, "Failed to retrieve document: id is null")
            return
        }

        val query = json {
            obj("_id" to id)
        }

        mongoClient.rxFind("documents", query)
            .subscribeBy(
                onSuccess = { it ->
                    if (it.size != 1) {
                        LOGGER.error("Failed to retrieve document, retrieved more or less than 1 document for id = $id!")
                        message.fail(CryptReplyErrorCodes.FAIL_TO_RETRIEVE, "Failed to retrieve document")
                    } else {
                        message.reply(it.first())
                    }
                },
                onError = {
                    LOGGER.error("Failed to retrieve document with id = $id", it)
                    message.fail(CryptReplyErrorCodes.FAIL_TO_RETRIEVE, "Failed to retrieve document: ${it.message}")
                }
            )
    }

    private fun getAllDocuments(message: Message<JsonObject>) {
        val query = json { obj() }

        mongoClient.rxFind("documents", query)
            .subscribeBy(
                onSuccess = { result ->
                    message.reply(json { array(result) })
                },
                onError = {
                    LOGGER.error("Failed to retrieve documents", it)
                    message.fail(CryptReplyErrorCodes.FAIL_TO_RETRIEVE, "Failed to retrieve document: ${it.message}")
                }
            )
    }
}
