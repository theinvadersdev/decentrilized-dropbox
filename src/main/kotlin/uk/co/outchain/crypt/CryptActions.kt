package uk.co.outchain.crypt

object CryptActions {
    const val SCHEDULE_CRYPT_DOCUMENT = "schedule-crypt-document"
    const val RETRIEVE_DOCUMENT_DATA_FROM_HASH = "retrieve-document-data-from-hash"
    const val RETRIEVE_DOCUMENT_DATA_BY_ID = "retrieve-document-data-by-id"
    const val RETRIEVE_ALL_DOCUMENTS = "retrieve-all-documents"
}
