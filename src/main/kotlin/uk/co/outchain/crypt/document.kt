package uk.co.outchain.crypt

import uk.co.outchain.Config
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.reactivex.core.Vertx
import org.apache.commons.codec.digest.DigestUtils
import java.io.File
import java.io.FileInputStream
import java.lang.RuntimeException
import java.util.*
import javax.crypto.Cipher
import io.vertx.reactivex.ext.mongo.MongoClient
import java.nio.file.Paths

data class InsertDocumentInput(
    val config: JsonObject,
    val publicKeys: List<InsertDocumentPublicKey>,
    val sourceFile: String,
    val fileName: String,
    val sha256sum: String,
    val isPublic: Boolean
)

data class InsertDocumentPublicKey(
    val identity: String,
    val path: String
)

data class InsertDocumentPublicKeyCipher(
    val identity: String,
    val cipher: Cipher
)

data class InsertDocumentResult(
    val payload: JsonObject? = null,
    val job: Flowable<DocumentStatus>? = null
)

enum class DocumentStatus(val key: String) {
    PREPARE(key = "prepare"),
    SCHEDULED(key = "scheduled"),
    PROCESSING(key = "processing"),
    ENCRYPTED(key = "encrypted"),
    FINISHED(key = "finished"),
    FAILED(key = "failed"),
    ;
}

class DocumentError(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

fun insertDocument(vertx: Vertx, mongoClient: MongoClient, input: InsertDocumentInput): Single<InsertDocumentResult> {
    return when {
        !File(input.sourceFile).exists() ->
            Single.error(DocumentError("cannot find document to encrypt"))

        DigestUtils.sha256Hex(FileInputStream(input.sourceFile)) != input.sha256sum ->
            Single.error(DocumentError("sha256sum does not match the file uploaded on the server"))

        input.publicKeys.isEmpty() && !input.isPublic ->
            Single.error(DocumentError("at least one public key must be provided"))

        else -> {
            insertValidatedDocument(vertx, mongoClient, input)
        }
    }
}

private fun insertValidatedDocument(vertx: Vertx, mongoClient: MongoClient, input: InsertDocumentInput): Single<InsertDocumentResult> {
    return if (input.isPublic) {
        val payload = json {
            obj(
                "sha256sum" to input.sha256sum,
                "filename" to input.fileName,
                "document_path" to input.sourceFile,
                "status" to DocumentStatus.PREPARE.key,
                "is_public" to input.isPublic
            )
        }

        mongoClient.rxInsert("documents", payload)
            .flatMap { id ->
                val source = Paths.get(input.sourceFile)

                val publicDocumentsPath = Config.publicDocumentRootPath(input.config)
                val dest = Paths.get(publicDocumentsPath, "$id-${input.fileName}")

                vertx.fileSystem().rxMove(source.toAbsolutePath().toString(), dest.toAbsolutePath().toString())
                    .toSingle { Pair(id, dest) }
            }
            .flatMap { (id, dest) ->
                val query = json {
                    obj("_id" to id)
                }

                val updateStatus = json {
                    obj(
                        "\$set" to obj(
                            "status" to DocumentStatus.FINISHED.key,
                            "document_path" to dest.toString()
                        )
                    )
                }

                payload.put("status", DocumentStatus.FINISHED.key)
                payload.put("document_path", dest.toString())

                mongoClient.rxUpdateCollection("documents", query, updateStatus)
            }
            .map {
                InsertDocumentResult(
                    payload = payload,
                    job = null
                )
            }
    } else {
        val payload = json {
            obj(
                "sha256sum" to input.sha256sum,
                "filename" to input.fileName,
                "file_to_encrypt" to input.sourceFile,
                "status" to DocumentStatus.SCHEDULED.key,
                "is_public" to input.isPublic
            )
        }

        mongoClient.rxInsert("documents", payload)
            .flatMap { id ->
                val keys = input.publicKeys.map {
                    InsertDocumentPublicKeyCipher(
                        it.identity,
                        loadPublicKeyCipher(it.path)
                    )
                }

                val masterKey = generateAESMasterKey()
                val encryptedMasterKeys = encryptMasterKeys(keys.map { it.cipher }, masterKey)
                val keyRootPath = Config.masterKeysPath(input.config)

                val masterKeys = encryptedMasterKeys
                    .zip(keys.map { it.identity })
                    .map { (encryptedMasterKey, identity) ->
                        val encryptedMasterKeyName = "$identity.${input.fileName}.rsa_ecb_oaepwithsha_256andmgf1padding.masterkey"
                        val keyPath = "$keyRootPath/$id-$encryptedMasterKeyName"

                        writeKeyToFile(keyPath, encryptedMasterKey)

                        JsonObject(
                            mapOf(
                                "identity" to identity,
                                "key_path" to keyPath,
                                "crypt_algorithm" to "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"
                            )
                        )
                    }

                val query = json {
                    obj("_id" to id)
                }

                val update = json {
                    obj(
                        "\$set" to obj(
                            "status" to DocumentStatus.SCHEDULED.key,
                            // Пока мы не закончим шифрование, мы будем хранить мастер ключ в базе данных,
                            // после завершения - мастер ключ удалится.
                            "processing_master_key" to String(Base64.getEncoder().encode(masterKey)),
                            "master_keys" to masterKeys
                        )
                    )
                }

                payload.put("master_keys", masterKeys)
                payload.put("processing_master_key", String(Base64.getEncoder().encode(masterKey)))
                payload.put("_id", id)

                mongoClient.rxUpdateCollection("documents", query, update)
            }
            .map {
                InsertDocumentResult(
                    payload = payload,
                    job = encryptionJob(mongoClient, payload, input.config)
                )
            }
    }
}

fun encryptionJob(mongoClient: MongoClient, payload: JsonObject, config: JsonObject): Flowable<DocumentStatus> =
    Flowable.create({ emitter ->
        val fileName = payload.getString("filename")
        val cryptAlgorithm = "AES/CBC/PKCS5PADDING"
            .replace("/", "_")
            .toLowerCase()

        val id = payload.getString("_id")

        val encryptedDocumentRootPath = Config.encryptedDocumentRootPath(config)
        val encryptedDocumentPath = "$encryptedDocumentRootPath/$id-$fileName.$cryptAlgorithm.enc"

        val query = json {
            obj("_id" to id)
        }

        val updateStatus = json {
            obj(
                "\$set" to obj(
                    "status" to DocumentStatus.PROCESSING.key,
                    "crypt_algorithm" to "AES/CBC/PKCS5PADDING",
                    "encrypted_document_path" to encryptedDocumentPath
                )
            )
        }

        mongoClient.rxUpdateCollection("documents", query, updateStatus)
            .map { emitter.onNext(DocumentStatus.PROCESSING) }
            .toFlowable()
            .concatMap {
                Flowable.fromCallable {
                    val masterKey = Base64.getDecoder().decode(payload.getString("processing_master_key"))

                    encryptToFile(
                        masterKey,
                        FileInputStream(File(payload.getString("file_to_encrypt"))),
                        encryptedDocumentPath
                    )
                }
            }
            .concatMap {
                val updateDocumentState = json {
                    obj(
                        "\$set" to obj(
                            "status" to DocumentStatus.FINISHED.key
                        ),
                        "\$unset" to obj(
                            "processing_master_key" to "",
                            "file_to_encrypt" to ""
                        )
                    )
                }
                mongoClient.rxUpdateCollection("documents", query, updateDocumentState)
                    .toFlowable()
            }
            .map {
                File(payload.getString("file_to_encrypt")).delete()
                emitter.onNext(DocumentStatus.FINISHED)
            }
            .subscribeBy(
                onNext = { emitter.onComplete() },
                onError = emitter::onError
            )

    }, BackpressureStrategy.DROP)
