package uk.co.outchain.crypt

object CryptReplyErrorCodes {
    const val NO_ACTION_SPECIFIED = 0
    const val BAD_ACTION = 1
    const val FAIL_TO_SCHEDULE = 2
    const val FAIL_TO_RETRIEVE = 3
}
