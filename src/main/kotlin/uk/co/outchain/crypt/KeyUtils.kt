package uk.co.outchain.crypt

import java.io.*
import java.nio.file.Files
import java.nio.file.Paths
import java.security.*
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher
import java.security.KeyFactory
import java.security.spec.PKCS8EncodedKeySpec
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.crypto.CipherOutputStream
import javax.crypto.CipherInputStream

fun generateRSAPair(keyLength: Int): KeyPair {
    val generator = KeyPairGenerator.getInstance("RSA")
    generator.initialize(keyLength)

    return generator.generateKeyPair()
}

fun getPublicKeyCipher(keyBytes: ByteArray): Cipher {
    val cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")
    val spec = X509EncodedKeySpec(keyBytes)
    val kf = KeyFactory.getInstance("RSA")
    val publicKey = kf.generatePublic(spec)

    cipher.init(Cipher.ENCRYPT_MODE, publicKey)
    return cipher
}

fun getPublicKeyCipher(key: PublicKey): Cipher =
    getPublicKeyCipher(key.encoded)

fun loadPublicKeyCipher(path: String): Cipher {
    val keyBytes = Files.readAllBytes(Paths.get(path))
    return getPublicKeyCipher(keyBytes)
}

fun loadPrivateKeyCipher(path: String): Cipher {
    val keyBytes = Files.readAllBytes(Paths.get(path))
    return getPrivateKeyCipher(keyBytes)
}

fun getPrivateKeyCipher(keyBytes: ByteArray): Cipher {
    val cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")
    val spec = PKCS8EncodedKeySpec(keyBytes)
    val kf = KeyFactory.getInstance("RSA")
    val privateKey = kf.generatePrivate(spec)

    cipher.init(Cipher.DECRYPT_MODE, privateKey)
    return cipher
}

fun getPrivateKeyCipher(key: PrivateKey): Cipher =
    getPrivateKeyCipher(key.encoded)

fun writeKeyToFile(path: String, key: ByteArray) {
    val f = File(path)
    f.parentFile.mkdirs()

    val fos = FileOutputStream(f)
    fos.write(key)
    fos.flush()
    fos.close()
}

fun encryptMasterKeys(encCiphers: List<Cipher>, masterKey: ByteArray): List<ByteArray> =
    encCiphers.map { it.doFinal(masterKey) }

fun decryptMasterKey(decCipher: Cipher, encrypted: ByteArray): ByteArray =
    decCipher.doFinal(encrypted)

data class AESCipher(
    val cipher: Cipher,
    val initVector: ByteArray
)

fun getDocumentEncryptCipher(masterKey: ByteArray): AESCipher {
    val initVector = ByteArray(16)
    val random = SecureRandom()
    random.nextBytes(initVector)

    val iv = IvParameterSpec(initVector)
    val masterKeySpec = SecretKeySpec(masterKey, "AES")

    val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    cipher.init(Cipher.ENCRYPT_MODE, masterKeySpec, iv)

    return AESCipher(cipher, initVector)
}

fun getDocumentDecryptCipher(masterKey: ByteArray, initVector: ByteArray): Cipher {
    val iv = IvParameterSpec(initVector)
    val masterKeySpec = SecretKeySpec(masterKey, "AES")

    val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    cipher.init(Cipher.DECRYPT_MODE, masterKeySpec, iv)

    return cipher
}

fun encryptDocumentData(cipher: Cipher, data: ByteArray): ByteArray =
    cipher.doFinal(data)

fun decryptDocumentData(cipher: Cipher, encryptedData: ByteArray): ByteArray =
    cipher.doFinal(encryptedData)

fun generateAESMasterKey(): ByteArray {
    val random = SecureRandom()
    val masterKey = ByteArray(32)

    random.nextBytes(masterKey)
    random.setSeed(32)

    return masterKey
}

fun encryptToFile(masterKey: ByteArray, content: InputStream, path: String) {
    val cipher = getDocumentEncryptCipher(masterKey)

    content.use { contentInputStream ->
        FileOutputStream(path).use { fileOut ->
            CipherOutputStream(fileOut, cipher.cipher).use { cipherOut ->
                fileOut.write(cipher.initVector)

                while (true) {
                    val buffer = ByteArray(4096)
                    val read = contentInputStream.read(buffer)

                    if (read <= 0)
                        break

                    cipherOut.write(buffer.sliceArray(IntRange(0, read - 1)))
                }
            }
        }
    }
}

inline fun <T> decryptFile(masterKey: ByteArray, path: String, crossinline use: (CipherInputStream) -> T): T {
    FileInputStream(path).use { fileIn ->
        val initVector = ByteArray(16)
        fileIn.read(initVector)

        val cipher = getDocumentDecryptCipher(masterKey, initVector)

        CipherInputStream(fileIn, cipher).use { cipherIn ->
            return use(cipherIn)
        }
    }
}
