package uk.co.outchain

import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.core.DeploymentOptions
import io.vertx.core.Future
import io.vertx.reactivex.config.ConfigRetriever
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.json.JsonObject

class MainVerticle : AbstractVerticle() {
    override fun start(startFuture: Future<Void>) {
        Single.merge(
            listOf(
                deployNode("envs/node1/config.json"),
                deployNode("envs/node2/config.json")
            )
        )
            .subscribeBy(
                onComplete = startFuture::complete,
                onError = startFuture::fail
            )
    }

    private fun deployNode(configPath: String): Single<String> {
        val configStore = ConfigStoreOptions()
            .setType("file")
            .setConfig(JsonObject().put("path", configPath))

        val options = ConfigRetrieverOptions().addStore(configStore)
        val retriever = ConfigRetriever.create(vertx, options)

        return retriever.rxGetConfig()
            .flatMap { config ->
                vertx
                    .rxDeployVerticle(
                        "uk.co.outchain.crypt.CryptVerticle",
                        DeploymentOptions()
                            .setConfig(config)
                            .setInstances(1)
                    )
                    .flatMap {
                        vertx.rxDeployVerticle(
                            "uk.co.outchain.http.HttpServerVerticle",
                            DeploymentOptions()
                                .setConfig(config)
                                .setInstances(1)
                        )
                    }
                    .flatMap {
                        vertx.rxDeployVerticle(
                            "uk.co.outchain.http.ClientVerticle",
                            DeploymentOptions()
                                .setConfig(config)
                                .setInstances(1)
                        )
                    }

            }
    }
}
